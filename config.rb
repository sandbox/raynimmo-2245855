# Require any additional compass plugins here.
# require 'susy'
#require 'modular-scale'
#require 'compass-normalize'

# Set this to the root of your project when deployed:
http_path = ".."
css_dir = "css"
sass_dir = "sass"
images_dir = "images"
javascripts_dir = "js"

# You can select your preferred output style here (can be overridden via the command line):
# output_style = :compressed (expanded. nested, compact)
output_style = :expanded

# To enable relative paths to assets via compass helper functions. Uncomment:
# relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
line_comments = true


# If you prefer the indented syntax, you might want to regenerate this
# project again passing --syntax sass, or you can uncomment this:
# preferred_syntax = :sass
# and then run:
# sass-convert -R --from scss --to sass sass scss && rm -rf sass && mv scss sass


# This is a quick helper function to make it easier to maintain regular and retina images in web sites/apps.
# https://github.com/joelambert/Retina-Compass-Helpers
# retina_ext = File.join(File.dirname(__FILE__), 'retina')
# require File.join(retina_ext, 'lib', 'sass_extensions.rb')
# add_import_path File.join(retina_ext, 'stylesheets')
