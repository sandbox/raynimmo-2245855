<!DOCTYPE html>
<html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"
  <head>
    <?php print $head; ?>
    <title><?php print $head_title; ?></title>
    <?php print $styles; ?>
    <?php print $scripts; ?>
  </head>
  <body class="<?php print $classes; ?>" <?php print $attributes;?>>
    <?php //print $page_top; ?>
    <?php //print $page; ?>
    <header id="header">
      <div class="container">
          <div class="row">

              <div class="logo">
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
                    <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>"/>
                  </a>
              </div>


          </div>
      </div>
    </header>
<section id="mainmenu">
    <nav id="menu" class="container">
        <?php print render($page['header']); ?>
    </nav>

</section>

<section id="container">
<div class="container">
  <div class="row">
  <section id="main-content">

    <?php if ($title): ?>
      <h1><?php print $title; ?></h1>
    <?php endif; ?>

    <?php print render($title_suffix); ?>
    <?php print $messages; ?>
    <?php print render($page['help']); ?>

    <?php if ($tabs): ?>
      <?php print render($tabs); ?>
    <?php endif; ?>

    <?php //print render($page['content']) ?>
    <?php print $content; ?>

  </section>  </div>

  <div class="divider-hor">
  </div>






</div>
</section>

<!--footer-->
<footer id="footer">
    <div class="container">
        <div class="row">
          <ul>
            <li><p><a href="user">Client Login</a><p></li>
            <li><p>&copy; 2008 - <?php echo Date('Y'); ?> - Jungle Creative</p></li>
          </ul>
        </div>
    </div>
</footer>

<!--footer menu-->
<section id="footer-menu">
    <div class="container">
        <div class="row">

        </div>
    </div>
</section>

  </body>
</html>
