<?php
  if (!isset($path)) {
    $nodepath = $_GET['q'];
  }

  if ($nodepath == 'contact') {
    $title = 'Get In Touch';
  }
  if ($nodepath == 'blog') {
    $title = 'Blog Header';
  }
?>
<script type="text/javascript">
    //$("[rel=tooltip]").tooltip();
</script>
<header id="header">
    <div class="container">
        <div class="row">

            <div class="logo">
              <?php if ($logo): ?>
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
                  <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>"/>
                </a>
              <?php endif; ?>
              <?php if ($site_name): ?>
                <h1><a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a></h1>
              <?php endif; ?>
              <?php if ($site_slogan): ?>
                <h2><?php print $site_slogan; ?></h2>
              <?php endif; ?>
            </div>



            <div class="span9 top-social">

                <a data-original-title="Facebook" rel="tooltip" data-placement="top" class="facebook"
                   href="#"></a>

                <a data-original-title="Twitter" rel="tooltip" data-placement="top" class="twitter2"
                   href="#"></a>

                <a data-original-title="Dribble" rel="tooltip" data-placement="top" class="dribbble"
                   href="#"></a>

                <a data-original-title="Digg" rel="tooltip" data-placement="top" class="digg"
                   href="#"></a>

                <a data-original-title="DeviantArt" rel="tooltip" data-placement="top" class="dart"
                   href="#"></a>

                <a data-original-title="Market" rel="tooltip" data-placement="top" class="market"
                   href="#"></a>

                <form class="top-search pull-right hidden-phone">
                    <div class="input-append">
                        <input placeholder="...keyword" type="text" id="appendedInputButton" class="span2">
                        <button type="button" class="btn">Search</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</header>
<section id="mainmenu">
    <nav id="menu" class="container">
      <?php if ($page['header']): ?>
        <?php print render($page['header']); ?>
      <?php endif; ?>
      <?php //if ($main_menu): ?>
        <?php //print theme('links', array('links' => $main_menu, 'attributes' => array('id' => 'primary', 'class' => array('links', 'clearfix', 'main-menu')))); ?>
      <?php //endif; ?>
    </nav>

</section>
<section>
<?php if ($page['highlight']): ?>
  <?php print render($page['highlight']) ?>
<?php endif; ?>
</section>
<section id="container">
<div class="container">
  <div class="row">
  <section id="main-content">

    <p><b>page-taxonomy.tpl.php</b></p>
    
    <?php echo $breadcrumb; ?>

    <?php //if ($title): ?>
      Entries Tagged with <h1><?php print $title; ?></h1>
    <?php //endif; ?>
    <?php
    $current = taxonomy_get_term(arg(2));
    echo $current->description;
    ?>

    <?php print render($title_suffix); ?>
    <?php print $messages; ?>
    <?php print render($page['help']); ?>

    <?php if ($tabs): ?>
      <?php print render($tabs); ?>
    <?php endif; ?>

    <?php if ($action_links): ?>
      <ul><?php print render($action_links); ?></ul>
    <?php endif; ?>

    <?php print render($page['content']) ?>
    <?php print $feed_icons; ?>
  </section>

  <?php if ($page['sidebar_first']): ?>
    <?php print render($page['sidebar_first']); ?>
  <?php endif; ?> <!-- /sidebar-first -->

  <?php if ($page['sidebar_second']): ?>
    <?php print render($page['sidebar_second']); ?>
  <?php endif; ?> <!-- /sidebar-second -->
  </div>

  <div class="divider-hor">
  </div>

  <div class="row highlights">
    <?php if ($page['content_bottom_first']): ?>
      <?php print render($page['content_bottom_first']); ?>
    <?php endif; ?>
    <?php if ($page['content_bottom_second']): ?>
      <?php print render($page['content_bottom_second']); ?>
    <?php endif; ?>
    <?php if ($page['content_bottom_third']): ?>
      <?php print render($page['content_bottom_third']); ?>
    <?php endif; ?>
    <?php if ($page['content_bottom_fourth']): ?>
      <?php print render($page['content_bottom_fourth']); ?>
    <?php endif; ?>
  </div>

  <?php if ($page['feature_footer']): ?>
  <div class="divider-hor">
      <span>Recent Portfolio Entries</span>
  </div>
  <div class="row portfrolio-feature">
    <div class="title-divider span12">
        <div class="divider-arrow"></div>
    </div>
      <?php print render($page['feature_footer']); ?>
  </div>
  <?php endif; ?>
</div>
</section>

<!--footer-->
<footer id="footer">
    <div class="container">
        <div class="row">
          <?php if ($page['footer']): ?>
            <?php print render($page['footer']); ?>
          <?php endif; ?>

            <div class="span3">
                <h3>Who is Jungle Creative?</h3>

                <p>Jungle Creative is the freelance web design and development portfolio of Ray Nimmo. I provide high quality full-service web design, web development, graphic and logo design for individuals, small businesses, service providers, and freelance professionals.</p>

                <p>I create robust and secure websites using standards based HTML and finely crafted CSS, alongside legible, semantic and structured code.</p>
            </div>
            <div class="span3">
                <h3>Flickr Photos</h3>
                <ul class="flickr clearfix"></ul>
            </div>
            <div class="span3">
                <h3>Contact Form</h3>

                <form id="contact" class="form-horizontal" method="post">
                    <div class="control-group">
                        <label class="control-label" for="inputName">Name</label>

                        <div class="controls">
                            <input type="text" id="inputName" placeholder="Name" name="inputName">
                            <label class="ferror" for="inputName" id="fname_error">Name is required.</label>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Email</label>

                        <div class="controls">
                            <input type="text" id="inputEmail" placeholder="Email" name="inputEmail">
                            <label class="ferror" for="inputEmail" id="femail_error">Email is required.</label>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputMessage"></label>

                        <div class="controls">
                            <textarea rows="3" id="inputMessage" name="inputMessage"></textarea>
                            <label class="ferror" for="inputMessage" id="fmessage_error">Message is required.</label>
                        </div>
                    </div>
                    <div class="submit-div">
                        <input type="submit" class="btn pull-right footer-button" value="SUBMIT!">
                    </div>
                </form>
            </div>
            <div class="span3">
                <h3>Address</h3>
                <address>
                    Jungle Creative<br />
                    Koh Phangan, 84280<br />
                    Thailand<br />
                    <i class="myicon-phone"></i>+66 (0)836-921850<br />
                    <i class="myicon-mail"></i>ray@junglecreative.com
                </address>
                Please use these details to contact me for any web related work.
            </div>
        </div>
    </div>
</footer>

<!--footer menu-->
<section id="footer-menu">
    <div class="container">
        <div class="row">
            <?php if ($page['page_footer_first']): ?>
              <div class="span8 hidden-phone">
              <?php print render($page['page_footer_first']); ?>
              </div>
            <?php endif; ?>
            <!--
            <div class="span8 hidden-phone">
                <?php //if ($secondary_menu): ?>
                  <?php //print theme('links', array('links' => $secondary_menu, 'attributes' => array('id' => 'secondary', 'class' => array('links', 'clearfix', 'sub-menu')))); ?>
                <?php //endif; ?>
            </div>
            -->
            <?php if ($page['page_footer_second']): ?>
              <div class="span4">
              <?php print render($page['page_footer_second']); ?>
              </div>
            <?php endif; ?>
            <!--
            <p class="span4"><span class="pull-right">Copyright 2008 - <?php echo Date('Y'); ?> - All Rights Reserved</span></p>
            -->
        </div>
    </div>
</section>