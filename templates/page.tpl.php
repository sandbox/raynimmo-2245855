<?php
  if (!isset($path)) {
    $nodepath = $_GET['q'];
  }

  if ($nodepath == 'contact') {
    $title = 'Get In Touch';
  }
  if ($nodepath == 'blog') {
    $title = 'Design &amp; Development Portfolio';
  }
  if ($nodepath == 'user/login') {
    $title = 'Client Login';
  }
  if ($nodepath == 'node/210') {
    $title = 'Being <em>Creative</em> in the <strong>Jungle</strong>';
  }
?>
<header id="header">
    <div class="container">
        <div class="row">
            <div class="logo">
              <?php if ($logo): ?>
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
                  <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>"/>
                </a>
              <?php endif; ?>
              <?php if ($site_name): ?>
                <h1><a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a></h1>
              <?php endif; ?>
              <?php if ($site_slogan): ?>
                <h2><?php print $site_slogan; ?></h2>
              <?php endif; ?>
            </div>
            <div class="span9 top-social">
                <?php if ($page['page_top_third']): ?>
                  <div class="page-top-third">
                  <?php print render($page['page_top_third']); ?>
                  </div>
                <?php endif; ?>
                <?php if ($page['page_top_second']): ?>
                  <div class="page-top-second">
                  <?php print render($page['page_top_second']); ?>
                  </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</header>
<section id="mainmenu">
    <nav id="menu" class="container">
      <?php if ($page['header']): ?>
        <?php print render($page['header']); ?>
      <?php endif; ?>
    </nav>
</section>
<section>
<?php if ($page['highlight']): ?>
  <?php print render($page['highlight']) ?>
<?php endif; ?>
</section>
<section id="container">
<div class="container">
  <div class="row">
  <section id="main-content">
    <?php //echo $breadcrumb; ?> 
    <?php if (taxonomy_term_load(arg(2))){ 
      $current = taxonomy_term_load(arg(2)); 
      $description = $current->description;
      ?>
      <?php if ($title): ?>
        <h1><small>Content tagged with:</small> <em><?php print $title; ?></em></h1>
      <?php endif; ?>
        <?php if ($description): ?>
          <div class="taxonomy-description messages status">
            <?php echo $current->description; ?>
          </div>
      <?php endif; ?>
    <?php }else{ ?>
      <?php if ($title): ?>
        <h1><?php print $title; ?></h1>
      <?php endif; ?>
    <?php } ?>

    <?php print render($title_suffix); ?>
    <?php print $messages; ?>
    <?php print render($page['help']); ?>

    <?php if ($tabs): ?>
      <?php print render($tabs); ?>
    <?php endif; ?>

    <?php if ($action_links): ?>
      <ul><?php print render($action_links); ?></ul>
    <?php endif; ?>

    <?php print render($page['content']) ?>
    <?php print $feed_icons; ?>
  </section>

  <?php if ($page['sidebar_first']): ?>
    <?php print render($page['sidebar_first']); ?>
  <?php endif; ?>

  <?php if ($page['sidebar_second']): ?>
    <?php print render($page['sidebar_second']); ?>
  <?php endif; ?> 
  </div>

  <div class="divider-hor">
  </div>

  <div class="row highlights">
    <?php if ($page['content_bottom_first']): ?>
      <?php print render($page['content_bottom_first']); ?>
    <?php endif; ?>
    <?php if ($page['content_bottom_second']): ?>
      <?php print render($page['content_bottom_second']); ?>
    <?php endif; ?>
    <?php if ($page['content_bottom_third']): ?>
      <?php print render($page['content_bottom_third']); ?>
    <?php endif; ?>
    <?php if ($page['content_bottom_fourth']): ?>
      <?php print render($page['content_bottom_fourth']); ?>
    <?php endif; ?>
  </div>

  <?php if ($page['feature_footer']): ?>
  <div class="divider-hor">
      <span>Recent Portfolio Entries</span>
  </div>
  <div class="row portfolio-feature">
    <div class="title-divider span12">
        <div class="divider-arrow"></div>
    </div>
      <?php print render($page['feature_footer']); ?>
  </div>
  <?php endif; ?>

  <?php if ($page['project_scroller']): ?>
          <div class="row">
            <div class="span3">
                <?php if ($page['project_description']): ?>
                <?php print render($page['project_description']); ?>
                <?php endif; ?>
            </div>
            <div class="span9">
                <div id="latest-work" class="carousel btleft">
                    <div class="carousel-wrapper">
                        <?php if ($page['project_scroller']): ?>
                          <?php print render($page['project_scroller']); ?>
                        <?php endif; ?>
                    </div>
                </div>
                <script type="text/javascript">
                    $(document).ready(function(){
                        $('#latest-work').elastislide({
                            imageW  : 270,
                            margin  : 30
                        });
                    });
                </script>
            </div>
        </div>
      <?php endif; ?>
</div>
</section>
<!--footer-->
<footer id="footer">
    <div class="container">
        <div class="row">
          <?php if ($page['footer']): ?>
            <?php print render($page['footer']); ?>
          <?php endif; ?>

            <div class="span3">
              <?php if ($page['footer_first']): ?>
                <?php print render($page['footer_first']); ?>
              <?php endif; ?>
            </div>
            <div class="span3">
                <?php if ($page['footer_second']): ?>
                <?php print render($page['footer_second']); ?>
              <?php endif; ?>
            </div>
            <div class="span3">
              <?php if ($page['footer_third']): ?>
                <?php print render($page['footer_third']); ?>
              <?php endif; ?>
            </div>
            <div class="span3">
              <?php if ($page['footer_fourth']): ?>
                <?php print render($page['footer_fourth']); ?>
              <?php endif; ?>
            </div>
        </div>
    </div>
</footer>
<section id="footer-menu">
    <div class="container">
        <div class="row">
            <?php if ($page['page_footer_first']): ?>
              <div class="span8">
              <?php print render($page['page_footer_first']); ?>
              </div>
            <?php endif; ?>
            <?php if ($page['page_footer_second']): ?>
              <div class="span4">
              <?php print render($page['page_footer_second']); ?>
              </div>
            <?php endif; ?>
        </div>
    </div>
</section>
